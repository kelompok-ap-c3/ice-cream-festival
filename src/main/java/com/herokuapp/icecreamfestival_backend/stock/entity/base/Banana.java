package com.herokuapp.icecreamfestival_backend.stock.entity.base;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public class Banana extends IceCream {

    public Banana() {
        description = "Banana";
        name = "Banana";
    }

    @Override
    public double cost() {
        return 2.5;
    }
}
