package com.herokuapp.icecreamfestival_backend.stock.entity.base;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public class SeaSalt extends IceCream {


    public SeaSalt() {
        description = "Sea Salt";
        name = "Sea Salt";
    }

    @Override
    public double cost() {
        return 3.0;
    }
}
