package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public class Marshmallow extends IceCream {
    IceCream iceCream;

    public Marshmallow(IceCream iceCream) {
        this.iceCream = iceCream;
        this.name = "Marshmallow";
    }

    @Override
    public String getDescription() {
        if(iceCream.getDescription().contains("with extra")==true){
            return iceCream.getDescription() + ", Marshmallow";
        }else return iceCream.getDescription() + " with extra, Marshmallow ";
    }

    @Override
    public double cost() {
        return 0.5 + iceCream.cost();
    }

}
