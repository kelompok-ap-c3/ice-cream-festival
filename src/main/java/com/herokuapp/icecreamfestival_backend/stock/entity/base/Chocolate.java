package com.herokuapp.icecreamfestival_backend.stock.entity.base;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public class Chocolate extends IceCream {

    public Chocolate() {
        description = "Chocolate";
        name = "Chocolate";
    }

    @Override
    public double cost() {
        return 2.0;
    }
}
