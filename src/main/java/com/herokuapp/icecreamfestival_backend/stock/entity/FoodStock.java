package com.herokuapp.icecreamfestival_backend.stock.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "foodstocks", schema = "public")
public class FoodStock {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonProperty("id")
    private int id;

    @Column(length = 50)
    @JsonProperty("name")
    private String name;

    @Column(name = "stock", nullable = false)
    @JsonProperty("stock")
    private int stock;

    @Column(nullable = false)
    @JsonProperty("cost")
    private double cost;

    public FoodStock(String name, double cost){
        this.name = name;
        this.stock = 0;
        this.cost = cost;
    }
    public FoodStock(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
