package com.herokuapp.icecreamfestival_backend.stock.entity.base;

import com.herokuapp.icecreamfestival_backend.stock.entity.IceCream;

public enum IceBaseProducer {
    CHOCOLATE,
    BANANA,
    SEASALT;

    public IceCream createIceCreamBase() {
        IceCream returnedIceCream = null;
        switch (this) {
            case BANANA:
                returnedIceCream = new Banana();
                break;

            case CHOCOLATE:
                returnedIceCream = new Chocolate();
                break;

            case SEASALT:
                returnedIceCream = new SeaSalt();
                break;

            default:
                returnedIceCream = new SeaSalt();
                break;
        }
        if (returnedIceCream.checkStock() < 1) {
            return null;
        }
        return returnedIceCream;
    }
}
