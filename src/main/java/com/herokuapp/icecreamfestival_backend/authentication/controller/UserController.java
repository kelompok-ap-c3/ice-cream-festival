package com.herokuapp.icecreamfestival_backend.authentication.controller;

import com.herokuapp.icecreamfestival_backend.authentication.entity.Role;
import com.herokuapp.icecreamfestival_backend.authentication.entity.User;
import com.herokuapp.icecreamfestival_backend.authentication.service.RoleService;
import com.herokuapp.icecreamfestival_backend.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @GetMapping
    public ResponseEntity<List<User>> findAll() {
        return ResponseEntity.ok(userService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Map<String, String> request) {
        String name = request.get("username");
        String email = request.get("email");
        String password = request.get("password");
        String role_name = request.get("roles");
        Role role = new Role(role_name);
        List<Role> roles = new ArrayList<>();
        roles.add(role);
        Boolean active = Boolean.parseBoolean(request.get("active"));

        User user = new User(name, email, password, active);
        return ResponseEntity.ok(userService.save(user));
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> findById(@PathVariable Long id) {
        Optional<User> user = userService.findById(id);
        if (!user.isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(user.get());
    }

    @GetMapping("/name/{username}")
    public ResponseEntity<User> findByName(@PathVariable String username) {
        Optional<User> user = userService.findByUsername(username);
        if (!user.isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(user.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> update(@PathVariable Long id, @Valid @RequestBody User user) {
        if (!userService.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(userService.save(user));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!userService.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        userService.deleteById(id);

        return ResponseEntity.ok().build();
    }

}
