package com.herokuapp.icecreamfestival_backend.authentication.controller;

import com.herokuapp.icecreamfestival_backend.authentication.entity.User;
import com.herokuapp.icecreamfestival_backend.authentication.repository.RoleRepository;
import com.herokuapp.icecreamfestival_backend.authentication.repository.UserRepository;
import com.herokuapp.icecreamfestival_backend.authentication.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
public class HomeController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/private")
    public ResponseEntity privateArea() {
        return ResponseEntity.ok("private");
    }

    @RequestMapping(value = "/api/topup", method = RequestMethod.POST)
    public ResponseEntity<?> top_up(@RequestBody Map<String, String> top_up) {
        HashMap<String, Object> response = new HashMap<>();
        String name = top_up.get("name");
        Double amount = Double.valueOf(top_up.get("amount"));
        Optional<User> opt_user = (Optional<User>) userRepository.findByUsername(name);
        User user = opt_user.get();
        user.setBalance(user.getBalance() + amount);
        userRepository.save(user);
        response.put("name", name);
        response.put("wallet", user.getBalance());
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

}
