package com.herokuapp.icecreamfestival_backend.authentication.repository;

import com.herokuapp.icecreamfestival_backend.authentication.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}