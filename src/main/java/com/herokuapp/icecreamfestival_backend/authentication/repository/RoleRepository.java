package com.herokuapp.icecreamfestival_backend.authentication.repository;

import com.herokuapp.icecreamfestival_backend.authentication.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String name);
}
