package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.base.Chocolate;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ChocochipTest {
    private Chocochip doubleChoco;

    @Before
    public void setUp() {
        doubleChoco = new Chocochip(new Chocolate());
    }

    @Test
    public void testMethodCost() {
        assertEquals(2.2, doubleChoco.cost(), 0.0);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("Chocolate with extra, Chocochip", doubleChoco.getDescription());
    }
}
