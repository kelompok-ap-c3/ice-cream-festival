package com.herokuapp.icecreamfestival_backend.stock.entity.topping;

import com.herokuapp.icecreamfestival_backend.stock.entity.base.SeaSalt;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PopcornTest {
    private Popcorn seasaltCaramel;

    @Before
    public void setUp() {
        seasaltCaramel = new Popcorn(new SeaSalt());
    }

    @Test
    public void testMethodCost() {
        assertEquals(3.3, seasaltCaramel.cost(), 0.0);
    }

    @Test
    public void testMethodGetDescription() {
        assertEquals("Sea Salt with extra, Popcorn", seasaltCaramel.getDescription());
    }
}
