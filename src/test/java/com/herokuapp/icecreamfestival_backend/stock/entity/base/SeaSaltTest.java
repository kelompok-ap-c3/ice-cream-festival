package com.herokuapp.icecreamfestival_backend.stock.entity.base;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SeaSaltTest {
    private SeaSalt seasalt;

    @Before
    public void setUp() {
        seasalt = new SeaSalt();
    }

    @Test
    public void cost() {
        assertEquals(3.0, seasalt.cost(), 0.0);
    }

    @Test
    public void getDescription() {
        assertEquals("Sea Salt", seasalt.getDescription());
    }
}