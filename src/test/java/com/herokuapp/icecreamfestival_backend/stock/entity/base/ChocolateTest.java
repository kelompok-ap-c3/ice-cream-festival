package com.herokuapp.icecreamfestival_backend.stock.entity.base;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ChocolateTest {
    private Chocolate chocolate;

    @Before
    public void setUp() {
        chocolate = new Chocolate();
    }

    @Test
    public void cost() {
        assertEquals(2.0, chocolate.cost(), 0.0);
    }

    @Test
    public void getDescription() {
        assertEquals("Chocolate", chocolate.getDescription());
    }
}